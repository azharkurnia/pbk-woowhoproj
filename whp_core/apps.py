from django.apps import AppConfig


class WhpCoreConfig(AppConfig):
    name = 'whp_core'
