from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^dashboard', dashboard, name='dashboard'),
    url(r'^forum', forum, name='forum'),
    url(r'^quiz', quiz, name='quiz'),
    url(r'^text', text, name='text'),
    url(r'^video', video, name='video'),
    url(r'^profile', profile, name='profile'),
]
