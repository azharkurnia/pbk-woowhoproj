from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
def index(request):
    html = "home.html"
    return render(request, html)

@csrf_exempt
def dashboard(request):
    html = "dashboard.html"
    return render(request, html)

@csrf_exempt
def forum(request):
    html = "forum.html"
    return render(request, html)

@csrf_exempt
def quiz(request):
    html = "quiz.html"
    return render(request, html)

@csrf_exempt
def text(request):
    html = "text.html"
    return render(request, html)

@csrf_exempt
def video(request):
    html = "video.html"
    return render(request, html)

@csrf_exempt
def profile(request):
    html = "profile.html"
    return render(request, html)